package org.elu.learn.cloudnative.reservationservice;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

@Component
public class ResourceReservation implements ResourceProcessor<Resource<Reservation>> {
  @Override
  public Resource<Reservation> process(Resource<Reservation> resource) {
    String url = "http://aws.images.com/" +
        resource.getContent().getId() + ".jpg";
    resource.add(new Link(url, "profile-photo"));
    return resource;
  }
}
