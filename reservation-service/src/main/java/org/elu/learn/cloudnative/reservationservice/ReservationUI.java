package org.elu.learn.cloudnative.reservationservice;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.material.Material;

@Route("ui")
@Theme(Material.class)
public class ReservationUI extends VerticalLayout {
  private ReservationRepository reservationRepository;
  private final Grid<Reservation> grid;

  public ReservationUI(ReservationRepository reservationRepository) {
    this.reservationRepository = reservationRepository;
    this.grid = new Grid<>(Reservation.class);
    add(grid);
    listCustomers();
  }

  private void listCustomers() {
    grid.setItems(reservationRepository.findAll());
  }
}
