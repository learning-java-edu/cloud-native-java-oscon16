package org.elu.learn.cloudnative.reservationservice;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class ReservationServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(ReservationServiceApplication.class, args);
  }

  @Bean
  CommandLineRunner commandLineRunner(ReservationRepository reservationRepository) {
    return (args) -> Arrays.stream("Edu,Janna,Arno,Lauri,Alisa,Karoliina".split(","))
                         .forEach(name -> reservationRepository.save(new Reservation(name)));
  }
}
